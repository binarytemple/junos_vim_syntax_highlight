prep:
	mkdir -p ~/.vim/ftdetect
	mkdir -p ~/.vim/syntax

link:	 prep 
	ln -s `pwd`/ftdetect/juniper.vim ~/.vim/ftdetect/juniper.vim 
	ln -s `pwd`/syntax/juniper.vim ~/.vim/syntax/juniper.vim  

clean:
	rm -f ~/.vim/ftdetect/juniper.vim 
	rm -f ~/.vim/syntax/juniper.vim 

install: clean 
	cp `pwd`/ftdetect/juniper.vim ~/.vim/ftdetect/
	cp `pwd`/syntax/juniper.vim ~/.vim/syntax/
